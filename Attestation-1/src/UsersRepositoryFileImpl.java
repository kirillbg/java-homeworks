import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class UsersRepositoryFileImpl {
    private List<String> lines;
    FileClass fileClass;
    public UsersRepositoryFileImpl() throws Exception {
        fileClass = new FileClass("User.txt");
        lines = new ArrayList<>();
        lines = fileClass.ReadFile();
    }
    public User findById(int id){
        try {
            for (String ln: lines) {
                String[] line = ln.split("\\|");
                if (Integer.parseInt(line[0]) == id){
                    User usr = new User(line[1], line[2], Integer.parseInt(line[3]), Boolean.parseBoolean(line[4]));
                    usr.Id = Integer.parseInt(line[0]);
                    return usr;
                }
            }
        } catch (Exception ignored){return null;}
        return null;
    }
    public void create(User user, int... id){

        String line = user.Name + "|" + user.LastName + "|" + user.Age + "|" + user.HasWork;
        if (id.length == 0 || id == null){
            if (lines.size() > 0) {
                int i = Integer.parseInt(lines.get(lines.size() - 1).split("\\|")[0]);
                user.Id = i+1;
            } else if (lines.isEmpty()) {
                user.Id = lines.size()+1;
            }
            line = user.Id + "|" +line;
            lines.add(line);
        }
        else if (id.length == 1) {
            user.Id = id[0];
            line = (user.Id+1) + "|" +line;
            lines.add(id[0], line);
        }
    }
    public void update(User user){
        lines.remove(user.Id-1);
        create(user, user.Id-1);
    }
    public void delete(User user){
        if (user != null) {
            User usr = findById(user.Id);
            if (usr != null && usr.Id == user.Id) {
                lines.remove(user.Id - 1);
            }
        }
    }
    public void save() throws Exception {
        System.out.println("Записи обновлены и сохранены");
        for (String line1 : lines) {
            System.out.println(line1);
        }
        fileClass.WriteFile(lines);
    }
}