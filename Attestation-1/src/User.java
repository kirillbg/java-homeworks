public class User {
    public int Id;
    public String Name;
    public String LastName;
    public int Age;
    public boolean HasWork;
    public User(String Name, String LastName, int Age, boolean HasWork){
        this.Name = Name;
        this.LastName = LastName;
        this.Age = Age;
        this.HasWork = HasWork;
    }
}
