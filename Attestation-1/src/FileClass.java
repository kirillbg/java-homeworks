import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileClass {
    private BufferedReader reader;
    private Path file;
    private String FileName;
    public FileClass(String fileName) throws Exception {
        FileName = fileName;
        file = Paths.get(fileName);
        if (Files.notExists(file)) {
            Files.write(file, new byte[]{}); //Создаём пустой файл
        }
    }
    public List<String> ReadFile() throws Exception {
        reader = new BufferedReader(new FileReader(FileName));
        List<String> ListLines = new ArrayList<>();
        String line = reader.readLine();

        while (line != null) {
            ListLines.add(line);
            line = reader.readLine();
        }

        return ListLines;
    }

    public void WriteFile(List<String> Data) throws Exception {
        Files.write(file, Data);
    }
}
