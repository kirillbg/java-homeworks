import java.sql.*;
import java.util.Properties;

public class Main {
    public static void main(String[] args) throws Exception {
        String url = "jdbc:postgresql://localhost:5432/school";
        String user = "postgres";
        String password = "8904";
        Class.forName("org.postgresql.Driver");
        Properties props = new Properties();
        props.setProperty("user", user);
        props.setProperty("password", password);
        props.setProperty("ssl", "false");
        Connection conn = DriverManager.getConnection(url, props);

        System.out.println("Соединение к БД успешно завершено!");
        System.out.println("Выполнение запросов...");

        Statement st = conn.createStatement();

        System.out.println("Вывод всех студентов и относящихся к ним преподавателей");
        for (int i = 1; i < 32; i++){
            System.out.println(getStudentInfo(st, i));
        }

        System.out.println("Вывод всех преподавателей и относящихся к ним студентов");
        for (int i = 1; i < 11; i++){
            System.out.println(getTeacherInfo(st, i));
        }

        st.close();
    }

    private static String getStudentInfo(Statement _st, int id) throws SQLException {
        return "\nК студенту " + getQuery(_st, "SELECT students.fullname FROM students WHERE id = " + id)
                + " относятся следующие преподаватели:\n" +
                getQuery(_st, "SELECT teachers.fullname as teacher " +
                        "FROM teachers, manytomany " +
                        "WHERE manytomany.student = "+ id +" AND manytomany.teacher = teachers.id");
    }

    private static String getTeacherInfo(Statement _st , int id) throws SQLException {
        return "\nК преподавателю " + getQuery(_st, "SELECT teachers.fullname FROM teachers WHERE id = " + id)
                + "относятся следующие студенты:\n" +
                getQuery(_st, "SELECT students.fullname as student\n" +
                        "FROM students, manytomany\n" +
                        "WHERE manytomany.teacher = "+ id +" AND manytomany.student = students.id");
    }

    private static String getQuery(Statement _st , String query) throws SQLException {
        String result = "";
        ResultSet rs = _st.executeQuery(query);
        while (rs.next()) {
            result += rs.getString(1) + "\n";
        }
        rs.close();
        return result;
    }
}