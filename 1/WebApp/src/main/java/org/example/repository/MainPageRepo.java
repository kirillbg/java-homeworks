package org.example.repository;

import org.example.entity.MainPageEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MainPageRepo extends JpaRepository<MainPageEntity, Integer> {
}
