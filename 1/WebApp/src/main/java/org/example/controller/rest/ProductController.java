package org.example.controller.rest;
import org.example.dto.MainPageDto;
import org.example.dto.ProductDto;
import org.example.service.MainPageService;
import org.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/api/Product")
public class ProductController {
    private final ProductService productsService;

    @Autowired
    public ProductController(ProductService productsService) {
        this.productsService = productsService;
    }

    @GetMapping
    public List<ProductDto> getAll() {
        return productsService.getAll();
    }

    @GetMapping("/{id}")
    public ProductDto getById(@PathVariable Integer id) {
        return productsService.getById(id);
    }

    @PostMapping
    public ProductDto create(@RequestBody ProductDto articlesDto) {
        return productsService.create(articlesDto);
    }

    @PutMapping
    public ProductDto update(@RequestBody ProductDto articlesDto) {
        return productsService.update(articlesDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Integer id) {
        productsService.deleteById(id);
    }
}
