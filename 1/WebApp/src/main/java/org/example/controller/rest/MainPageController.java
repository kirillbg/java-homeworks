package org.example.controller.rest;

import org.example.dto.MainPageDto;
import org.example.service.MainPageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/MainPage")
public class MainPageController {
    private final MainPageService articlesService;

    @Autowired
    public MainPageController(MainPageService articlesService) {
        this.articlesService = articlesService;
    }

    @GetMapping
    public List<MainPageDto> getAll() {
        return articlesService.getAll();
    }

    @GetMapping("/{id}")
    public MainPageDto getById(@PathVariable Integer id) {
        return articlesService.getById(id);
    }

    @PostMapping
    public MainPageDto create(@RequestBody MainPageDto articlesDto) {
        return articlesService.create(articlesDto);
    }

    @PutMapping
    public MainPageDto update(@RequestBody MainPageDto articlesDto) {
        return articlesService.update(articlesDto);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Integer id) {
        articlesService.deleteById(id);
    }
}
