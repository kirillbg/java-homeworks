package org.example.controller.rest;
import org.example.dto.AuthDto;
import org.example.entity.UserEntity;
import org.example.service.UserService;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.csrf.CsrfToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("/api/auth")
@Slf4j
public class AuthController {
    private final UserService userService;

    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody AuthDto authDto, HttpServletRequest request) {

        if (request.getUserPrincipal() != null) {
            return ResponseEntity.badRequest().build();
        }

        try {
            request.login(authDto.getUsername(), authDto.getPassword());
        } catch (ServletException e) {
            return ResponseEntity.badRequest().build();
        }

        Authentication auth = (Authentication) request.getUserPrincipal();
        UserEntity userEntity = (UserEntity) auth.getPrincipal();

        log.info("User {} logged in", userEntity.getUsername());

        CsrfToken csrf = (CsrfToken) request.getAttribute("_csrf");
        return ResponseEntity.ok(csrf);
    }
    @PostMapping("/register")
    public void register(@RequestBody AuthDto authDto) {
        userService.register(authDto);
    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest httpServletRequest) {
        try {
            httpServletRequest.logout();
        } catch (ServletException e) {
            log.error("Error during logout", e);
        }
    }
}
