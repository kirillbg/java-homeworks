package org.example.controller.rest;

import org.example.dto.OrderDto;
import org.example.dto.ProductDto;
import org.example.entity.ProductEntity;
import org.example.entity.UserEntity;
import org.example.service.OrderService;
import org.example.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/Order")
public class OrderController {
    private final OrderService orderService;
    @Autowired
    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }
    @PostMapping
    public OrderDto create(@RequestBody OrderDto articlesDto) {
        articlesDto.setUser((UserEntity) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        return orderService.create(articlesDto);
    }
}
