package org.example.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MainPageDto {
    private Integer id;
    private String title;
    private String text;
    private String author;
}
