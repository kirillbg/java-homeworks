package org.example.dto;

import org.example.entity.ProductEntity;
import org.example.entity.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    private Integer id;
    private UserEntity user;
    private ProductEntity product;
}
