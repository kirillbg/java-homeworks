package org.example.service;

import org.example.dto.MainPageDto;
import org.example.entity.MainPageEntity;
import org.example.repository.MainPageRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MainPageService {
    private final MainPageRepo articlesRepo;

    @Autowired
    public MainPageService(MainPageRepo articlesRepo) {
        this.articlesRepo = articlesRepo;
    }

    public List<MainPageDto> getAll() {
        return articlesRepo.findAll().stream().map(this::convertMainPageEntityToMainPageDto).toList();
    }

    public MainPageDto getById(Integer id) {

        return convertMainPageEntityToMainPageDto(articlesRepo.findById(id).orElseThrow(() -> new RuntimeException(String.format("Articles by id: %s not found", id))));
    }

    public MainPageDto create(MainPageDto articlesDto) {
        MainPageEntity saved = articlesRepo.save(convertMainPageDtoToMainPageEntity(articlesDto));
        return convertMainPageEntityToMainPageDto(saved);
    }

    public MainPageDto update(MainPageDto articlesDto) {
        if (articlesRepo.findById(articlesDto.getId()).isEmpty()) {
            throw new RuntimeException(String.format("Articles by id: %s not found", articlesDto.getId()));
        }
        MainPageEntity saved = articlesRepo.save(convertMainPageDtoToMainPageEntity(articlesDto));
        return convertMainPageEntityToMainPageDto(saved);
    }

    public void deleteById(Integer id) {
        articlesRepo.deleteById(id);
    }

    private MainPageDto convertMainPageEntityToMainPageDto(MainPageEntity articlesEntity) {
        return new MainPageDto(
                articlesEntity.getId(),
                articlesEntity.getTitle(),
                articlesEntity.getText(),
                articlesEntity.getAuthor()
        );
    }

    private MainPageEntity convertMainPageDtoToMainPageEntity(MainPageDto articlesDto) {
        return new MainPageEntity(
                articlesDto.getId(),
                articlesDto.getTitle(),
                articlesDto.getText(),
                articlesDto.getAuthor()
        );
    }
}
