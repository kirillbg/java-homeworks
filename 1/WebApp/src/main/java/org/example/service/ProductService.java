package org.example.service;

import org.example.dto.ProductDto;
import org.example.entity.ProductEntity;
import org.example.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class ProductService {
    private final ProductRepo productRepo;
    @Autowired
    public ProductService(ProductRepo articlesRepo) {
        this.productRepo = articlesRepo;
    }

    public List<ProductDto> getAll() {
        return productRepo.findAll().stream().map(this::convertProductEntityToProductDto).toList();
    }

    public ProductDto getById(Integer id) {

        return convertProductEntityToProductDto(productRepo.findById(id).orElseThrow(() -> new RuntimeException(String.format("Articles by id: %s not found", id))));
    }

    public ProductDto create(ProductDto articlesDto) {
        ProductEntity saved = productRepo.save(convertProductDtoToProductEntity(articlesDto));
        return convertProductEntityToProductDto(saved);
    }

    public ProductDto update(ProductDto articlesDto) {
        if (productRepo.findById(articlesDto.getId()).isEmpty()) {
            throw new RuntimeException(String.format("Articles by id: %s not found", articlesDto.getId()));
        }
        ProductEntity saved = productRepo.save(convertProductDtoToProductEntity(articlesDto));
        return convertProductEntityToProductDto(saved);
    }

    public void deleteById(Integer id) {
        productRepo.deleteById(id);
    }
    private ProductDto convertProductEntityToProductDto(ProductEntity articlesEntity) {
        return new ProductDto(
                articlesEntity.getId(),
                articlesEntity.getName(),
                articlesEntity.getDescription(),
                articlesEntity.getPrice()
        );
    }

    private ProductEntity convertProductDtoToProductEntity(ProductDto articlesDto) {
        return new ProductEntity(
                articlesDto.getId(),
                articlesDto.getName(),
                articlesDto.getDescription(),
                articlesDto.getPrice()
        );
    }
}
