package org.example.service;
import org.example.dto.MainPageDto;
import org.example.dto.OrderDto;
import org.example.dto.ProductDto;
import org.example.entity.MainPageEntity;
import org.example.entity.OrderEntity;
import org.example.entity.ProductEntity;
import org.example.repository.MainPageRepo;
import org.example.repository.OrderRepo;
import org.example.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class OrderService {
    private final OrderRepo orderRepo;
    @Autowired
    public OrderService(OrderRepo articlesRepo) {
        this.orderRepo = articlesRepo;
    }
    public OrderDto create(OrderDto articlesDto) {
        OrderEntity saved = orderRepo.save(convertOrderDtoToOrderEntity(articlesDto));
        return convertOrderEntityToOrderDto(saved);
    }
    private OrderDto convertOrderEntityToOrderDto(OrderEntity orderEntity) {
        return new OrderDto(
                orderEntity.getId(),
                orderEntity.getUser(),
                orderEntity.getProduct()
        );
    }
    public OrderEntity convertOrderDtoToOrderEntity(OrderDto articlesDto) {
        return new OrderEntity(
                articlesDto.getId(),
                articlesDto.getUser(),
                articlesDto.getProduct()
        );
    }
}
