import java.io.*;
import java.util.List;
import java.util.stream.Collectors;

public class HumanStreamExample {
    public static void main(String[] args) throws IOException {
        final String FILE_NAME = "Human.txt";
        File file = new File(FILE_NAME);

        file.createNewFile();

        BufferedWriter bw = new BufferedWriter(new FileWriter(FILE_NAME));

        bw.write("1|Budchenko|Kirill|19|true\n");
        bw.write("2|Suvorov|Igor|25|true\n");
        bw.write("3|Demkin|Alexsandr|17|false\n");
        bw.write("4|Ivashenko|Artem|20|tue\n");

        bw.flush();

        BufferedReader br = new BufferedReader(new FileReader(FILE_NAME));

        List<Human> humans = br.lines()
                .map(stroke ->{
                    String[] info = stroke.split("\\|");
                    int id = Integer.parseInt(info[0]);
                    String lastname = info[1];
                    String firstname = info[2];
                    int age = Integer.parseInt(info[3]);
                    boolean hasWork = Boolean.parseBoolean(info[4]);
                    Human human = new Human(id,firstname,lastname,age,hasWork);
                    return  human;
                })
                .collect(Collectors.toList());

        System.out.println("До изменения: ");
        for (int i = 0; i < humans.size();i++){
            System.out.println(humans.get(i));
        }

        /*public User findById(int id) {
            List<User> users = usfi.readFileOfUsers();
            User user = users.stream()
                    .filter(n -> n.getId() == id)
                    .findFirst()
                    .orElse(null);
            if (user == null) {
                System.out.println("User not found");
            }
            return user;
        }*/

       public Human findById(int id;) {
            List<Human> users = usfi.readFileOfUsers();
            Human user = users.stream()
                    .filter(n -> n.getId() == id)
                    .findFirst()
                    .orElse(null);
            if (user == null) {
                System.out.println("User not found");
            }
            return human;
        }

    }
}
