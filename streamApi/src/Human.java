public class Human {
    private int id;
    private String firstName;
    private String lastname;
    private int age;

    private boolean hasWork;

    public Human(int id, String firstName, String lastname, int age, boolean hasWork){
        this.firstName = firstName;
        this.lastname = lastname;
        this.age = age;
        this.id = id;
        this.hasWork = hasWork;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isHasWork() {
        return hasWork;
    }

    public void setHasWork(boolean hasWork) {
        this.hasWork = hasWork;
    }

    public boolean equals(Object o){
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return id == human.id;
    }
    @Override
    public String toString() {
        return id + " " + lastname + " " + firstName + " " + age + "" + hasWork;
    }
}
