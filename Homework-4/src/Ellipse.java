public class Ellipse extends Figure {
    int smallAxis = 7;
    int bigAxis = 4;
    double pi = 3.14;

    @Override
    public void getPerimeter() {
        System.out.println("Периметр Ellips = " + pi * (smallAxis + bigAxis));
    }
}
