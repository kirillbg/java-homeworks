public class Main {
    public static void main(String[] args) {
        Ellipse ellipse = new Ellipse();
        Rectangle rectangle = new Rectangle();
        Square square = new Square();
        Circle circle = new Circle();

        Figure[] figures = new Figure[4];
        figures[0] = ellipse;
        figures[1] = rectangle;
        figures[2] = square;
        figures[3] = circle;
        for (int i = 0; i < figures.length; i++)
            figures[i].getPerimeter();
    }
}