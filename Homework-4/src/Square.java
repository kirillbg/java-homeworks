public class Square extends Rectangle implements Moveable {
    int side = 3;

    @Override
    public void getPerimeter() {
        System.out.println("Периметр Square = " + 4 * side);
    }

    @Override
    public void move(int k, int l) {
        this.x = this.x + k;
        this.y = this.y + l;
        System.out.println("Square:");
        System.out.println("Координата x изменена на " + this.x);
        System.out.println("Координата y изменена на " + this.y);
    }
}