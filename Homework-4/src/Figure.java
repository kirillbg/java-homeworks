public abstract class Figure {
    int x = 0;
    int y = 0;

    public abstract void getPerimeter();
}
