public class Circle extends Ellipse implements Moveable{

    double pi = 3.14;
    int radius = 4;

    @Override
    public void getPerimeter() {
        System.out.println("Периметр Circle = " + (2 * pi * radius));
    }

    @Override
    public void move(int k, int l) {
        this.x = this.x + k;
        this.y = this.y + l;
        System.out.println("Circle:");
        System.out.println("Координата x изменена на " + this.x);
        System.out.println("Координата y изменена на " + this.y);
    }
}
