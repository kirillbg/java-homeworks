public class Rectangle extends Figure {
    int smallSide = 5;
    int bigSide = 6;
    @Override
    public void getPerimeter() {
        System.out.println("Периметр Rectangle = " + (2 * smallSide + 2 * bigSide));
    }
}
