import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a = Math.abs(scanner.nextInt());
        int b = 0;
        int c = 0;
        if (a != 0){
            while (a > 0){
                c = a % 10;
                a /= 10;
                if (c > b){
                    b = c;
                }
            }
        }
        System.out.println(b);
    }
}