import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] array = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        int result = getIndexofNumber(array,18);
        System.out.println(result);
        System.out.println(Arrays.toString(array));
        sortArray(array);
        System.out.println(Arrays.toString(array));
    }

    public static void sortArray(int[] array){
        int[] sortArray = new int[array.length];
        int numberArray = 0;
        for (int i = 0; i < array.length; i++){
            if (array[i] != 0){
                sortArray[numberArray] = array[i];
                numberArray++;
            }
        }
        for (int i = 0; i < array.length; i++){
            array[i] = sortArray[i];
        }
    }

    public static int getIndexofNumber(int[] array, int number) {
        int index = 0;
        int i;
        for (i = 0; i < array.length; i++) {
            index++;
            if (array[i] == number) return index;
        }

        return -1;
    }
}
